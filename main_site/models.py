from django.db import models
from django.urls import reverse


# Create your models here.
class PostSummary(models.Model):
    postcode_area = models.CharField(max_length=2)
    area_covered = models.CharField(max_length=40)
    population = models.IntegerField(default=0)
    households = models.IntegerField(default=0)
    postcodes = models.IntegerField(default=0)
    active_postcodes = models.IntegerField(default=0)
    non_geographic_postcodes = models.IntegerField(default=0)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)

    class Meta:
        ordering = ['postcode_area']

    def __str__(self):
        return self.postcode_area


class PostCodeDistrict(models.Model):
    postcode_district = models.CharField(max_length=20)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    easting = models.IntegerField(default=0)
    northing = models.IntegerField(default=0)
    grid_reference = models.CharField(max_length=50)
    town_area = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    postcodes = models.IntegerField(default=0)
    active_postcodes = models.IntegerField(default=0)
    population = models.IntegerField(default=0)
    households = models.IntegerField(default=0)
    nearby_districts = models.CharField(max_length=255)

    class Meta:
        ordering = ['postcode_district']

    def __str__(self):
        return self.postcode_district


class PostCodeSector(models.Model):
    postcode_sector = models.CharField(max_length=20)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    easting = models.IntegerField(default=0)
    northing = models.IntegerField(default=0)
    grid_reference = models.CharField(max_length=50)
    postcodes = models.IntegerField(default=0)
    active_postcodes = models.IntegerField(default=0)
    population = models.IntegerField(default=0)
    households = models.IntegerField(default=0)

    class Meta:
        ordering = ['postcode_sector']

    def __str__(self):
        return self.postcode_sector


class Postcode(models.Model):
    postcode = models.CharField(max_length=20)
    in_use = models.CharField(max_length=5)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    easting = models.IntegerField(default=0)
    northing = models.IntegerField(default=0)
    grid_reference = models.CharField(max_length=50)
    county = models.CharField(max_length=255)
    district = models.CharField(max_length=200)
    ward = models.CharField(max_length=200)
    district_code = models.CharField(max_length=200)
    ward_code = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    county_code = models.CharField(max_length=200)
    constituency = models.CharField(max_length=200)
    introduced = models.DateField(blank=True, null=True)
    terminated = models.DateField(blank=True, null=True)
    parish = models.CharField(max_length=200)

    class Meta:
        ordering = ['postcode']

    def __str__(self):
        return self.postcode

    def get_absolute_url(self):
        return reverse('addresses_for', kwargs={'pk': self.postcode})




class Address(models.Model):
    name = models.CharField(max_length=355)

    def __str__(self):
        return self.name

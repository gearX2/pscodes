from django.conf.urls import url, include

from . import views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^postcode-area/(?P<pk>[a-zA-Z0-9]+)/$', views.AreaPage.as_view(), name='area_page'),
    url(r'^postcode-district/(?P<pk>[a-zA-Z0-9]+)/$', views.DistrictPage.as_view(), name='district_page'),
    url(r'^postcode-sector/(?P<pk>[a-zA-Z0-9 ]+)/$', views.SectorPage.as_view(), name='sector_page'),
    url(r'^addresses-for/(?P<pk>[a-zA-Z0-9 ,-]+)/$', views.AddressesPage.as_view(), name='addresses_for'),
    url(r'^search/', include('haystack.urls')),

]

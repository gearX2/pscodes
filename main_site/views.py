from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import (
    Address,
    PostSummary,
    PostCodeDistrict,
    PostCodeSector,
    Postcode
)

# Create your views here.
class IndexView(ListView):
    model = PostSummary
    paginate_by = 100
    context_object_name = 'post_summaries'
    template_name = 'main_site/index.html'


class AreaPage(ListView):
    model = PostCodeDistrict
    template_name = 'main_site/area_page.html'
    context_object_name = 'post_districts'
    paginate_by = 100

    def get_queryset(self):
        return PostCodeDistrict.objects.filter(postcode_district__startswith=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(AreaPage, self).get_context_data(**kwargs)
        context['area_code'] = self.kwargs['pk']
        context['count'] = self.get_queryset().count()

        return context



class DistrictPage(ListView):
    model = PostCodeSector
    template_name = 'main_site/district_page.html'
    context_object_name = 'post_sectors'
    paginate_by = 100

    def get_queryset(self):
        return PostCodeSector.objects.filter(postcode_sector__startswith=self.kwargs['pk'])


    def get_context_data(self, **kwargs):
        context = super(DistrictPage, self).get_context_data(**kwargs)
        context['district_code'] = self.kwargs['pk']
        context['count'] = self.get_queryset().count()

        return context


class SectorPage(ListView):
    model = Postcode
    template_name = 'main_site/sector_page.html'
    context_object_name = 'postcodes'
    paginate_by = 100

    def get_queryset(self):
        return Postcode.objects.filter(postcode__startswith=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(SectorPage, self).get_context_data(**kwargs)
        context['sector_code'] = self.kwargs['pk']
        context['count'] = self.get_queryset().count()
        return context


class AddressesPage(ListView):
    model = Address
    template_name = 'main_site/addresses_page.html'
    context_object_name = 'addresses'
    paginate_by = 100

    def get_queryset(self):
        return Address.objects.filter(name__icontains=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super(AddressesPage, self).get_context_data(**kwargs)
        context['postcode'] = Postcode.objects.get(postcode=self.kwargs['pk'])
        context['count'] = self.get_queryset().count()
        return context

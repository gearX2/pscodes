from django.contrib import admin

from .models import (
    Address,
    PostSummary,
    PostCodeDistrict,
    PostCodeSector,
    Postcode
)


# Register your models here.
admin.site.register(Address)
admin.site.register(PostSummary)
admin.site.register(PostCodeDistrict)
admin.site.register(PostCodeSector)
admin.site.register(Postcode)

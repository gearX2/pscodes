import datetime
from haystack import indexes
from .models import Postcode


class PostcodeIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    country = indexes.CharField(model_attr='country')
    ward = indexes.CharField(model_attr='ward')


    def get_model(self):
        return Postcode

import re
# run this in python manage.py shell

def run():
    import csv
    from main_site.models import Address
    with open('path_to_/sample100k.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row['Address']
            name = name.replace('_', ',')
            name = name.replace('-', ' ')
            name = re.sub('[.%&amp&#!?]', '', name)
            the_instance = Address(
                name=name
            )

            the_instance.save()

import datetime
# run this in python manage.py shell

def run():
    import csv
    from main_site.models import Postcode
    with open('path_to/Postcodes.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            postcode = row['Postcode']
            postcode = postcode.upper()
            in_use = row['In Use?']
            latitude = row['Latitude']
            try:
                latitude = float(latitude)
            except ValueError:
                latitude = 0
            longitude = row['Longitude']
            try:
                longitude = float(longitude)
            except ValueError:
                longitude = 0
            easting = row['Easting']
            northing = row['Northing']
            grid_reference = row['Grid Ref']
            county = row['County']
            district = row['District']
            ward = row['Ward']
            ward_code = row['Ward Code']
            district_code = row['District Code']
            country = row['Country']
            county_code = row['County Code']
            constituency = row['Constituency']
            introduced = row['Introduced']
            introduced_filtered = datetime.datetime.strptime(introduced, '%d-%m-%Y')
            terminated = row['Terminated']
            terminated_filtered = datetime.datetime.strptime(terminated, '%d-%m-%Y')
            terminated_filtered
            parish = row['Parish']

            the_instance = Postcode(
                postcode=postcode,
                in_use=in_use,
                latitude=latitude,
                longitude=longitude,
                easting=easting,
                northing=northing,
                grid_reference=grid_reference,
                county=county,
                district=district,
                ward=ward,
                district_code=district_code,
                ward_code=ward_code,
                country=country,
                county_code=county_code,
                constituency=constituency,
                introduced=introduced_filtered.strftime('%Y-%m-%d'),
                terminated=terminated_filtered.strftime('%Y-%m-%d'),
                parish=parish
            )

            the_instance.save()

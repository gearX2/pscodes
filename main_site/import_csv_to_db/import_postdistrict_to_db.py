
# run this in python manage.py shell

def run():
    import csv
    from main_site.models import PostCodeDistrict
    with open('path_to/Postcode-district.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            postcode_district = row['Postcode District']
            latitude = row['Latitude']
            try:
                latitude = float(latitude)
            except ValueError:
                latitude = 0
            longitude = row['Longitude']
            try:
                longitude = float(longitude)
            except ValueError:
                longitude = 0
            easting = row['Easting']
            northing = row['Northing']
            grid_reference = row['Grid Reference']
            town_area = row['Town/Area']
            region = row['Region']
            postcodes = row['Postcodes']
            active_postcodes = row['Active postcodes']
            population = row['Population']
            if not population:
                population = 0
            households = row['Households']
            if not households:
                households = 0
            nearby_districts = row['Nearby districts']

            the_instance = PostCodeDistrict(
                postcode_district=postcode_district,
                latitude=latitude,
                longitude=longitude,
                easting=easting,
                northing=northing,
                grid_reference=grid_reference,
                town_area=town_area,
                region=region,
                postcodes=postcodes,
                active_postcodes=active_postcodes,
                population=population,
                households=households,
                nearby_districts=nearby_districts
            )

            the_instance.save()


# run this in python manage.py shell

def run():
    import csv
    from main_site.models import PostCodeSector
    with open('path_to/Postcode-sector.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            postcode_sector = row['Postcode Sector']
            latitude = row['Latitude']
            try:
                latitude = float(latitude)
            except ValueError:
                latitude = 0
            longitude = row['Longitude']
            try:
                longitude = float(longitude)
            except ValueError:
                longitude = 0
            easting = row['Easting']
            northing = row['Northing']
            grid_reference = row['Grid Ref']
            postcodes = row['Postcodes']
            if not postcodes:
                postcodes = 0
            active_postcodes = row['Active postcodes']
            if not active_postcodes:
                active_postcodes = 0
            population = row['Population']
            if population == '':
                population = 0
            households = row['Households']
            if households == '':
                households = 0

            the_instance = PostCodeSector(
                postcode_sector=postcode_sector,
                latitude=latitude,
                longitude=longitude,
                easting=easting,
                northing=northing,
                grid_reference=grid_reference,
                postcodes=postcodes,
                active_postcodes=active_postcodes,
                population=int(population),
                households=int(households),
            )

            the_instance.save()

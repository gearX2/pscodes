
# run this in python manage.py shell

def run():
    import csv
    from main_site.models import PostSummary
    with open('path_to/Postcodes-Summary.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            postcode_area = row['Postcode area']
            area_covered = row['Area covered']
            population = row['Population']
            if population == '':
                population = 0
            households = row['Households']
            if households == '':
                households = 0
            postcodes = row['Postcodes']
            if not postcodes:
                postcodes = 0
            active_postcodes = row['Active postcodes']
            if not active_postcodes:
                active_postcodes = 0
            non_geographic_postcodes = row['Non-geographic postcodes']

            latitude = row['Latitude']
            try:
                latitude = float(latitude)
            except ValueError:
                latitude = 0
            longitude = row['Longitude']
            try:
                longitude = float(longitude)
            except ValueError:
                longitude = 0

            the_instance = PostSummary(
                postcode_area=postcode_area,
                area_covered=area_covered,
                population=int(population),
                households=int(households),
                postcodes=postcodes,
                active_postcodes=active_postcodes,
                non_geographic_postcodes=non_geographic_postcodes,
                latitude=latitude,
                longitude=longitude,
            )

            the_instance.save()
